export default class BookstoteService {

    data = [
        {
            id: 1,
            title: 'JavaScript Everywhere',
            author: 'Adam D. Scott',
            price: 35,
            coverImage: 'https://balka-book.com/files/2019/11_13/12_03/u_files_store_6_12774.jpg'
        },
        {
            id: 2,
            title: 'Learning React Native',
            author: 'Bonnie Eisenman',
            price: 45,
            coverImage: 'https://techrocks.ru/wp-content/uploads/2019/12/Learning-React-Native.jpg'
        }
    ];
    
    getBooks() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if(Math.random() > 0.75) {
                    reject(new Error('Something goes wrong'));
                }
                resolve(this.data)
            }, 700);
        });
    }
};