import React from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';

import './shop-header.css';

const ShopHeader = ({shoppingCart: {cartItems, orderTotal}}) => {

    let tootalCount = 0;

    cartItems.forEach(element => {
        tootalCount += element.count;
    });

    return (
        <header className="shop-header row">
            <Link to="/">
                <div className="logo text-dark">React Store</div>
            </Link>
            <Link to="/cart">
                <div className="shopping-cart">
                    <i className="cart-icon fa fa-shopping-cart" />
                    {tootalCount} items (${orderTotal})
                </div>
            </Link>
        </header>
    )
};

const mapStateToProps = (state) => {
    const { shoppingCart } = state;
    return { shoppingCart }
};

export default connect(mapStateToProps)(ShopHeader);